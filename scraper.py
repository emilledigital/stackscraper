"""Scraper

This module contains the entry point for the scraper library.

Usage:
    cron example
    windows scheduler example

"""

import csv
import pprint
import time
from bs4 import BeautifulSoup

from generators.page_generator import pages
from generators.parsed_data import parsed_data
from helpers.get_beautifulsoup_result_set import return_result_set
from save.csv import head_up_new_csv, append_record_to_csv
from wrappers.trace import log_scrape_event
from config import FIELDS, WRR_FIELDS, URLS

import os

@log_scrape_event
def start_scrape():
    CWD = os.getcwd()
    DATA_DIR = os.path.join(CWD, 'data/')

    CSVNAME = DATA_DIR + 'data-stackoverflow-' + time.strftime('%Y-%m-%d', time.localtime()) + '.csv'

    head_up_new_csv(CSVNAME, FIELDS)

    url = URLS.get('visa-remote')

    entries_made = 0

    for page in pages(url, *('div', 'class', 's-pagination')):
        res_set = return_result_set(page, *('div', 'class', '-job-summary'))

        for record in parsed_data(res_set):
            append_record_to_csv(CSVNAME,record)
            entries_made += 1


if __name__ == '__main__':
     start_scrape()
