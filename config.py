FIELDS = [
    'id',
    'job_title',
    'company_name',
    'job_location',
    'date_posted',
    'technologies',
    ]

WRR_FIELDS = [
    'job_title',
    'company_name',
    'url',
]

URLS = {
    "visa-remote":"https://stackoverflow.com/jobs?r=true&v=true",
    "visa": "https://stackoverflow.com/jobs?v=true&sort=i&pg=",
    "remote":"https://stackoverflow.com/jobs?r=true",
    "we-work-remotely":"https://weworkremotely.com/categories/remote-programming-jobs",
    "homepage": "https://stackoverflow.com/",
}

PAGE_ELEMENTS = {
    "job-summary":{'element':('div', 'class', '-job-summary')}
}

if __name__ == '__main__':
    print (repr(__name__))
