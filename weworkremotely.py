import csv
import pprint
import time
from bs4 import BeautifulSoup
from generators.page_generator import pages
from generators.parsed_data import parsed_data
from helpers.get_beautifulsoup_result_set import return_result_set
from helpers.find import get_text
from save.csv import head_up_new_csv, append_record_to_csv
from wrappers.trace import log_scrape_event
from config import FIELDS, WRR_FIELDS, URLS
from pprint import pprint
import os

def handle_tags(bs4ElementTag):
    res = []
    for tag in bs4ElementTag:
        #pprint (get_text(tag))
        res.append(get_text(tag))

    tags = ','.join(res)
    return tags


def url_generator(url):
    base_url    = 'https://weworkremotely.com'
    res_set     = return_result_set(url, *('section', 'id', 'category-2'))
    results     = res_set[0]

    url_here    = results.find_all('li', {'class':'feature'})

    for item in url_here:

        links = item.find_all('a', href=True)

        for item in links:
            slug = item['href']

            if slug.startswith('/remote-jobs/'):
                final_url = base_url + slug
                yield final_url


def records(url):
    pagesoup = return_result_set(url, *('div', 'class', 'content'))
    page_here = pagesoup[0]

    header_element_tag = page_here.find('div', {'class': 'listing-header-container'})
    companyname_element_tag = page_here.find('div', {'class': 'company-card'})
    apply_element_tag = page_here.find('div', {'class': 'company-card'})
    listingtag_element_tag = page_here.find_all('span', {'class':'listing-tag'})

    job_title_here = header_element_tag.find('h1')
    company_name_here = companyname_element_tag.find('h2')
    apply_here = apply_element_tag.find('a', {'id': 'job-cta-alt-2'})

    job_title = get_text(job_title_here)

    company_name = get_text(company_name_here)
    apply_link = apply_here['href']
    listing_tags = handle_tags(listingtag_element_tag)

    return (job_title, company_name, listing_tags, apply_link)


@log_scrape_event
def scrape_wrr():
    CWD = os.getcwd()
    DATA_DIR = os.path.join(CWD, 'data/')

    CSVNAME = DATA_DIR + 'data-weworkremotely' + time.strftime('%Y-%m-%d', time.localtime()) + '.csv'

    head_up_new_csv(CSVNAME, WRR_FIELDS)

    url = URLS.get('we-work-remotely')

    entries_made = 0

    for url in url_generator(url):
        print (records(url))
        entries += 1



if __name__=='__main__':
    url = URLS.get('we-work-remotely')
    #scrape_wrr()
    listingurl = 'https://weworkremotely.com/remote-jobs/invitae-software-engineer'

    for link in url_generator(url):
        print (records(link))
