# stackscraper
> A scraper for Stack Overflow Jobs

## Getting started
### Welcome

Run scraper
```bash
python3 -m scraper
```

## To do
  * Start Django API
  * Revise namedtuples and implement
  * Write function for computing the date posted (relativedelta)



### Installation

Create and enter directory

```bash
mkdir dir
cd dir
```

Create a virtual environment.

```bash
python3 -m venv env
```

Activate virtual environment

```bash
source env/bin/activate
```

Clone repository

```bash
git clone https://emilledigital@bitbucket.org/emilledigital/stackscraper.git
```

Install dependencies

```
pip install -r requirements.txt
```

---

## Authors
  * Emille G. - [Twitter](http://twitter.com/emilledigital), [Blog](https://egxdigital.wordpress.com)


## Acknowledgements
  -
