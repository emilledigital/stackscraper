"""Trace

Wrappers that track program execution time, log events and more.

Objects/Exports
--------------
trace(func)
    Wrapper that prints the function name to console

trace_timer(func)
    Wrapper that prints the time elapsed since the last function call.
    Use it on scraper on entry point.

log_scrape_event(func)
    Wrapper that creates a timestamped record in a database.
    Use it on scraper entry point.

"""
import requests
import time
import functools

def trace(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        print(f'Calling <{func.__name__}>')
        result = func(*args, **kwargs)
        return result
    return wrapper

def trace_timer(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(f'Done! Elapsed: {end-start:.2F}s')
        return result
    return wrapper

def log_scrape_event(func):
    @functools.wraps(func)
    def wrapper():
        start = time.time()
        result = func()
        end = time.time()

        # Connect to database and update Scrape

        print(f'Scrape complete! Elapsed: {end-start:.2F}s')
        return result
    return wrapper
