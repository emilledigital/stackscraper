"""Validate

Contains wrappers for text cleaning operations.
To be used on BeautifulSoup find methods and string attributes

Usage:
    @return_valid
    do_find(resultSetElement, html, attr, val)

    @return_nospace_string
    get_string(find_result)

    @return_nospace_string
    get_text(find_result)

"""

import functools

def return_valid(func):
    """Replaces NULL values with the string 'N/A'"""
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        if result is not None:
            return result
        else:
            return 'N/A'
    return wrapper

def return_nospace_string(func):
    """Takes a BS dictionary key and returns a string clean of leading/trailing and extra spaces"""
    @functools.wraps(func)
    def wrapper(*args):
        white_space_chars = ' \r\n\t\f\v-'
        lead_space_stripped = func(*args).strip(white_space_chars)
        splitted = lead_space_stripped.split()
        buf = [e for e in splitted if e not in (' \r\n\t\f\v')]
        interpolated_spaces_stripped = ' '.join(buf)
        return interpolated_spaces_stripped
    return wrapper

if __name__ == '__main__':
    print (repr(__name__))
