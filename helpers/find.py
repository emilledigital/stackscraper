"""Find

A set of helper methods for calling BeautifulSoup.find and find_all on result set elements.
Text validations should be applied here as wrappers.

BeautifulSoup.find returns a dictionary, so a dictionary lookup is required to get the data in some cases.

For example:
    id_here = result_set_elem.find(html, attr, val)
    id = id_here[attr]


Objects/Exports
---------------
do_find()
    Calls the find method on a result set member

do_find_from()
    Calls the find method on a result object obtained from a do_find() call

do_find_all()
    Calls the find_all method on a result set member

get_string()
    Returns the string attribute of a result object obtained from a previous find call

get_text()
    Returns the string attribute of a result object obtained from a previous find call
"""

from wrappers.validate import *

@return_valid
def do_find(resultSetElement, html, attr, val):
    return resultSetElement.find(html, {attr:val})

@return_valid
def do_find_from(findobject, html, attr, val):
    return findobject.find(html, {attr:val})

@return_valid
def do_find_all(resultSetElement, html, attr, val):
    return resultSetElement.find_all(html, {attr:val})

@return_nospace_string
def get_string(find_result):
    return find_result.string

@return_nospace_string
def get_text(find_result):
    return find_result.text
