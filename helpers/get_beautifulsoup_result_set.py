"""GetBeautifulSoupResultSet

Get a BeautifulSoup result set with exception handling.

Usage:
    set = return_result_set(url, *(html,attr,val))
    for record in find_all_then_parse(set):
        save record ...

"""
import requests
from bs4 import BeautifulSoup
from wrappers.trace import trace, trace_timer
from pprint import pprint

#@trace_timer
#@trace
def return_result_set(url, html, attr, val):
    """Takes a url and returns a BeautifulSoup Result Set.
    Usage: URL, HTML parent element and selector.
    """
    try:
        _r = requests.get(url)
    except requests.exceptions.ConnectionError as ce:
        print(ce)
    except requests.exceptions.HTTPError as httperr:
        print(httperr)
    except requests.exceptions.TooManyRedirects as tmr:
        print(tmr)
    except requests.exceptions.Timeout as tmo:
        print(tmo)
    else:
        #pprint (_r.text)
        _soup = BeautifulSoup(_r.text, features="lxml")
        _res = _soup.find_all(html, {attr: val})
        #print (_res)
        return _res

if __name__ =='__main__':
    url = "https://stackoverflow.com/jobs?v=true"
    print ((return_result_set(url, *('div', 'class', '-job-summary'))))
