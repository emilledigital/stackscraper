"""ParsedData

A generator for creating the final records to save to file or database.

Usage:
    result_set = get_result_set(page, *(html:str, attr:str, val:str))
    for record in parsed_data(result_set):
        save record ...

"""

from collections import namedtuple
from config import FIELDS
from wrappers.trace import trace, trace_timer
from helpers.find import do_find, do_find_all, do_find_from, get_string, get_text

@trace
@trace_timer
def parsed_data(resultSet):
    """ Takes a BeautifulSoup result set and returns a list containing the values for each field in a single row"""

    for jobsummary in resultSet:

        """GET ITEMS"""

        id_here = jobsummary.find('span', {'class':'fav-toggle ps-absolute l16 c-pointer js-fav-toggle'})

        # title is inside a link which is inside of an h2
        title_elem = jobsummary.find('h2', {'class', 'fs-body2 job-details__spaced mb4'})
        title_here = title_elem.find('a', {'class','s-link s-link__visited job-link'})

        # company name and job location is inside a span inside a div with class -company
        company_name_elem = jobsummary.find('div',{'class', '-company'})
        company_name_span = company_name_elem.span
        job_location_span = company_name_elem.find('span',{'class', 'fc-black-500'})

        # date_posted is inside a span inside a spanb inside of div with class -title
        date_posted_elem = jobsummary.find('div', {'class', '-title'})
        date_posted_span = date_posted_elem.find('span',{'class', 'fc-black-500'})

        # tags are inside links inside a div inside the div with class -job-summary

        techs_elem = jobsummary.find('div',{'class', 'mt12 -tags'})
        tech_links = techs_elem.find_all('a', {'class', 'post-tag job-link no-tag-menu'})


        """LOOKUP ITEMS FOR DATA"""
        # Extract an attribute value to get the ID
        id = id_here['data-jobid']

        # get_text(title_here) or title_here['title'] or title_here.get('text')
        job_title = get_text(title_here)

        # get_text() to strip away leading and trailing whitespaces
        company_name = get_text(company_name_span)
        job_location = get_text(job_location_span)

        # placeholder for date posted
        date_posted = get_text(date_posted_span)

        # placeholder for technologies
        technologies = ','.join([get_text(tech) for tech in tech_links])

        #row = namedtuple('ROW', FIELDS)

        yield [
                id,
                job_title,
                company_name,
                job_location,
                date_posted,
                technologies,
              ]

if __name__ == '__main__':
    from helpers.get_beautifulsoup_result_set import return_result_set

    url = "https://stackoverflow.com/jobs?v=true"
    res_set = return_result_set(url, *('div', 'class', '-job-summary'))
    for row in parsed_data(res_set):
        print (row)
