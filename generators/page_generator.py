"""GetPages

A generator function for requesting each page in a search result.

Usage: Sites that have page number navigation links at the bottom.
"""

import requests
from bs4 import BeautifulSoup
from config import URLS
from wrappers.trace import trace_timer, trace
from helpers.find import do_find
from helpers.get_beautifulsoup_result_set import return_result_set

@trace
def pages(url, html, attr, val):
    """ Takes a URL and an HTML element and yields the page to scrape."""

    pagination_div = return_result_set(url, html, attr, val)

    try:
        a_tags = pagination_div[0].find_all('a', {'href':True})
        url_format = URLS["homepage"] + [a['href'] for a in a_tags][1][:-1]
        pages = [a.span.text.strip() for a in a_tags][:-1]
    except IndexError as e:
        print (e, '\n', 'Possible site change detected')
    else:
        last_page = int(pages[-1])

    for n in range(1,last_page+1):
        yield (url_format + str(n))

if __name__ == '__main__':
    target_url = URLS["visa-remote"]

    for page in pages(target_url, 'div', 'class', 'pagination'):
        print (page)
