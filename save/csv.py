"""CSV

Functions for heading up a CSV and appending a single row to the file.

Objects/Exports
-------
head_up_new_csv(name:str, fields:iterable)
    Heads up a new csv file with a collection of fieldnames

append_record_to_csv(name:str, records:iterable)
    Appends a new row of data to an existing csv file

"""

import csv

def head_up_new_csv(name,fields):
    """Prepares a CSV file with a header containing fields.
    """
    with open(name, 'w+') as csv_file:
        writer = csv.writer(csv_file, delimiter=',', lineterminator='\n')
        writer.writerow(fields)

def append_record_to_csv(name, record):
    """Writes a single row to a CSV file that already contains a header.
    """
    with open(name, 'a') as fd:
        writer = csv.writer(fd, delimiter=',', lineterminator='\n')
        writer.writerow(record)
