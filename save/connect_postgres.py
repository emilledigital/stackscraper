""" Excellent opportunity to implement a context manager
"""
from contextlib import contextmanager
import psycopg2

@contextmanager
def yield_cursor():
    try:
        connection = psycopg2.connect(
                                        user='engineerdata',
                                        password='1234',
                                        host='127.0.0.1',
                                        port='5432',
                                        database='globaljobs',
                                        )
        cursor = connection.cursor()
        print ( connection.get_dsn_parameters(),"\n")
        cursor.execute("SELECT version();")
        record = cursor.fetchone()
        print("You are connected to - ", record,"\n")
        yield cursor

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)

    finally:
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

@contextmanager
def yield_cursor_and_commit():
    """NOTICE: WRITE TWO VERSIONS OF THIS FUNCTION"""
    try:
        connection = psycopg2.connect(
                                        user='engineerdata',
                                        password='1234',
                                        host='127.0.0.1',
                                        port='5432',
                                        database='globaljobs',
                                        )
        cursor = connection.cursor()
        print ( connection.get_dsn_parameters(),"\n")
        cursor.execute("SELECT version();")
        record = cursor.fetchone()
        print("You are connected to - ", record,"\n")
        yield cursor

    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)

    finally:
        if(connection):
            connection.commit()
            print("Table created successfully in PostgreSQL")
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

if __name__ == '__main__':
    create_table = """CREATE TABLE job_posts (
                      job_id INT PRIMARY KEY NOT NULL,
                      company_id INT NOT NULL,
                      job_title TEXT NOT NULL,
                      job_location TEXT,
                      date_posted TEXT,
                      date_scraped DATE NOT NULL DEFAULT CURRENT_DATE,
                      job_page_slug TEXT);"""

    with yield_postgres_cursor() as cursor:
        cursor.execute(create_table)
